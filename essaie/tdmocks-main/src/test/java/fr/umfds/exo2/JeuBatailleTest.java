package fr.umfds.exo2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class JeuBatailleTest {

    @Mock
    JeuDeCartes mockJDC;

    @Test
    void testRoundSimple() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(13, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jb = new JeuBataille(mockJDC);

        Boolean result = jb.round();

        assertTrue(result);
        }

    @Test
    void testRound1Bataille() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(10, Couleur.coeur));

        deck2.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(13, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        Boolean result = jeuBataille.round();

        assertTrue(result);
        }
@Test
    void testRound2Bataille() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(10, Couleur.coeur));

        deck2.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(13, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jb1 = new JeuBataille(mockJDC);

        Boolean result = jb1.round();

        assertTrue(result);
        }

@Test
    void testRoundEarlyEnd() throws Exception, CarteIncorrecteException {
            ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(1, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        Boolean result = jeuBataille.round();

        assertTrue(result);
        }
@Test
    void testNumJoueurGagnant2() throws Exception, CarteIncorrecteException {
            ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.numJoueurGagnant();

        assertEquals(2, result);
        }

@Test
    void testNumJoueurGagnant1() throws Exception, CarteIncorrecteException {
            ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(2, Couleur.pique));
        deck2.addLast(new Carte(1, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.numJoueurGagnant();

        assertEquals(1, result);
        }
@Test
    void testNoNumJoueurGagnant() throws Exception, CarteIncorrecteException {
            ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.numJoueurGagnant();

        assertEquals(-1, result);
        }

@Test
    void testTailleJeuJoueur1() throws Exception, CarteIncorrecteException{
            ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.tailleJeuJoueur1();

        assertEquals(1, result);
        }
@Test
    void testTailleJeuJoueur2() throws Exception, CarteIncorrecteException{
            ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.tailleJeuJoueur2();

        assertEquals(2, result);
        }
        }