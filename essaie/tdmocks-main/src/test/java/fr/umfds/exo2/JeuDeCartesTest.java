package fr.umfds.exo2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.OngoingStubbing;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class JeuDeCartesTest {


    @InjectMocks
    JeuDeCartes jdc;

    @Mock
    JeuDeCartes jeuDeCartes;

    Random mockRandom = mock(Random.class, withSettings().withoutAnnotations());

    @Test
    void testDistribution2Deck3Cards() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();
        ArrayDeque<Carte> deck1=new ArrayDeque<Carte>(); //paquets n°1
        ArrayDeque<Carte> deck2=new ArrayDeque<Carte>(); //paquets n°2

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck1.addLast(new Carte(3, Couleur.pique));

        deck2.addLast(new Carte(13, Couleur.trefle));
        deck2.addLast(new Carte(12, Couleur.trefle));
        deck2.addLast(new Carte(11, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockRandom.nextInt(anyInt())).thenReturn(0, 0, 0, 48, 47, 46);

        ArrayList<ArrayDeque<Carte>> result = jdc.distribution(3, 2);

        for(int i = 0; i < 2; i++)
            for(int j = 0; j < 3; j++) {
                assertTrue((BooleanSupplier) result);
            }// une assertion pour vérifier si le nouveaux asquets qu'on a distiruber correspond bien au pâquet initial
    }

    @Test
    void testPiocheLastCard() throws CarteIncorrecteException {
        when(mockRandom.nextInt(52)).thenReturn(51);


        Carte p = jdc.pioche();

        assertEquals(13, p.getHauteur());
        assertEquals(Couleur.trefle, p.getCouleur());
    }

    @Test
    void testPiocheFirstCard() throws CarteIncorrecteException {
        when(mockRandom.nextInt(52)).thenReturn(0);

        Carte result = jdc.pioche();

        assertEquals(1, result.getHauteur());
        assertEquals(Couleur.pique, result.getCouleur());
    }
}