import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows ;

public class TestMonome  {

    private Monome m;
    private Monome s;

    @org.junit.jupiter.api.Test
    public void TestToString() {

       m = new Monome(4,1);
       assertEquals(m.toString(), "+4X");

       s = new Monome (4,1);
       assertEquals(s.toString(),"4x");
    }

    @Test
    public void TestConstrucDeg(){
        assertThrows(DegreNegatifException.class , ()-> {
            new Monome(4,-1);}
        );
    }


}
